<?php

/**
 * @file
 * Administration page callback.
 */

/**
 * IDPay configuration form
 */
function idpay_config_form($form, &$form_state) {

  $form['idpay_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('IDPay'),
    '#collapsed' => false,
    '#collapsible' => false,
  );

  $form['idpay_config']['idpay_config_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('Your API Key from idpay.ir.'),
    '#default_value' => variable_get('idpay_config_api_key', ''),
    '#required' => true,
  );

  $form['idpay_config']['idpay_config_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint'),
    '#description' => t('Endpoint path without method name.'),
    '#default_value' => variable_get(
      'idpay_config_endpoint',
      'https://api.idpay.ir/v1/'
    ),
    '#required' => true,
  );

  $form['idpay_config']['idpay_config_sandbox_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Available Sandbox Mode'),
    '#description' => t('Use sandbox mode for test gateway service.'),
    '#default_value' => variable_get('idpay_config_sandbox_mode', false),
  );

  return system_settings_form($form);
}

/**
 * IDPay payments
 */
function idpay_payments_page() {
  $query = db_select('idpay_payment', 'p')->extend('PagerDefault');
  $query->fields('p')
        ->limit(20)
        ->orderBy('p.created', 'DESC');

  $payments = $query->execute()->fetchAll();

  foreach ($payments as $row => $payment) {
    $user = user_load($payment->uid);
    $rows[] = array(
      'track_id'  => $payment->track_id,
      'user'      => theme('username', array('account' => $user)),
      'amount'    => number_format($payment->amount),
      'status'    => ucfirst($payment->status),
      'sandbox'   => $payment->sandbox ? '&#10004;' : '',
      'created'   => format_date($payment->created, 'custom', 'Y-m-d H:i:s'),
    );
  }

  $headers = array(t('Track id'), t('User'), t('Amount'), t('Status'), t('SandBox'), t('Created'));

  $table_data = array(
    'header'     => $headers,
    'rows'       => $rows,
    'sticky'     => true,
    'empty'      => t('No results found'),
    'attributes' => array(),
    'caption'    => count($rows) . ' rows',
    'colgroups'  => array()
  );

  $output = array(
    'content' => array(
      '#markup' => theme_table($table_data),
    ),
    'pager' => array(
      '#markup' => theme('pager', array('tags' => array(), 'quantity' => 5))
    ),
  );

  return $output;
}