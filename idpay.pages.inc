<?php

/**
 * @file
 * Payment page callback.
 */

use Idpay\Idpay;

require_once idpay_get_library_path() . '/vendor/autoload.php';

/**
 * IDPay payment form
 */
function idpay_payment_form($form, &$form_state) {

  $form['amount'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Amount'),
    '#description'    => t('Amount to be pay.'),
    '#default_value'  => 0,
    '#field_suffix'   => 'IRR',
    '#required'       => true,
    '#size'           => 20,
  );

  $form['name'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Your name'),
    '#required' => true,
    '#size'     => 30,
  );

  $form['phone'] = array(
    '#type'   => 'textfield',
    '#title'  => t('Phone'),
    '#size'   => 20,
  );

  $form['description'] = array(
    '#type'   => 'textfield',
    '#title'  => t('Description'),
    '#size'   => 60,
  );

  $form['submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Payment'),
  );

  return $form;
}

/**
 * Validate payment form
 */
function idpay_payment_form_validate($form, &$form_state) {
  $amount = $form_state['values']['amount'];

  if (empty($amount) || $amount <= 0) {
    form_set_error('amount', t('Invalid amount value.'));
  }
}

/**
 * Submit payment form
 */
function idpay_payment_form_submit($form, &$form_state) {
  global $user;

  $idpay = new Idpay(
    variable_get('idpay_config_api_key'),
    variable_get('idpay_config_endpoint'),
    variable_get('idpay_config_sandbox_mode', false)
  );

  $detail = array(
    'name'        => $form_state['values']['name'],
    'phone'       => $form_state['values']['phone'],
    'description' => $form_state['values']['description'],
  );

  $pid = db_insert('idpay_payment')
    ->fields(array(
      'uid'       => $user->uid,
      'amount'    => $form_state['values']['amount'],
      'track_id'  => 0,
      'status'    => 'created',
      'sandbox'   => (int) variable_get('idpay_config_sandbox_mode', false),
      'detail'    => serialize($detail),
      'created'   => REQUEST_TIME,
    ))
    ->execute();

  $payment = $idpay->payment(
    url('idpay/payment/return', array('absolute' => true)),
    $pid,
    $form_state['values']['amount'],
    $form_state['values']['name'],
    $form_state['values']['phone'],
    $form_state['values']['description']
  );

  if ($payment) {
    db_update('idpay_payment')
      ->fields(array(
        'track_id' => $idpay->getTrackId(),
        'status' => 'pending',
      ))
      ->condition('pid', $pid)
      ->execute();

    $idpay->gotoPaymentPath();
  }
  else {
    drupal_set_message(t('Can not communicate with payment gateway at this time.'), 'warning');
  }
}

/**
 * Return payment result handler.
 */
function idpay_payment_return() {
  $idpay = new Idpay(
    variable_get('idpay_config_api_key'),
    variable_get('idpay_config_endpoint'),
    variable_get('idpay_config_sandbox_mode', false)
  );

  if ($idpay->receiveData()) {
    $payment = db_select('idpay_payment', 'p')
      ->fields('p')
      ->condition('track_id', $idpay->getTrackId())
      ->range(0, 1)
      ->execute()->fetchObject();
    
    if ($payment) {
      if ($payment->status == 'completed') {
        return t('This payment has already been completed.');
      }

      $inquiry = $idpay->inquiry($payment->track_id, $payment->pid, $payment->amount);

      if ($inquiry['Status'] == 'success') {
        $states = array(
          1 => array(
            'value' => 'pending',
            'message' => t('Not paid yet.')
          ),
          2 => array(
            'value' => 'failed',
            'message' => t('Returned transaction amount.')
          ),
          3 => array(
            'value' => 'failed',
            'message' => t('Failed payment, Try again.')
          ),
          100 => array(
            'value' => 'completed',
            'message' => t('Your track id : %pid', array('%pid' => $payment->pid))
          ),
        );

        if (isset($states[$inquiry['Result']['status']])) {
          $status = $states[$inquiry['Result']['status']]['value'];
          $message = $states[$inquiry['Result']['status']]['message'];

          if ($status == 'completed' && $inquiry['Success']) {
            drupal_set_message(t('Your payment has been completed successfully.'));
          }
          else {
            drupal_set_message(t('There is a problem when paying.'), 'warning');
          }

          db_update('idpay_payment')
            ->fields(array('status' => $status))
            ->condition('pid', $payment->pid)
            ->execute();

          return $message;
        }
      }
      else {
        drupal_set_message(t('Not available service.'), 'error');
      }
    }
  }

  return t("Don't receive data.");
}