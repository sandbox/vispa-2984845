<?php

/**
 * @file
 * Hook implementations.
 */

 /**
 * Implements hook_menu().
 */
function idpay_menu() {

  // Payment form
  $items['idpay/payment'] = array(
    'title' => t('IDPay Payment'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('idpay_payment_form'),
    'access arguments' => array('access idpay payment'),
    'file' => 'idpay.pages.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  // Payment return
  $items['idpay/payment/return'] = array(
    'title' => t('IDPay payment return'),
    'page callback' => 'idpay_payment_return',
    'access arguments' => array('access idpay payment'),
    'file' => 'idpay.pages.inc',
    'type' => MENU_CALLBACK,
  );

  // Administration.
  $items['admin/config/services/idpay'] = array(
    'title' => 'IDPay',
    'description' => '',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
    'position' => 'right',
  );

  // Administration configs.
  $items['admin/config/services/idpay/config'] = array(
    'title' => 'IDPay configs',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('idpay_config_form'),
    'access arguments' => array('access administration pages'),
    'file path' => drupal_get_path('module', 'idpay'),
    'file' => 'idpay.admin.inc',
  );

  // Payment list.
  $items['admin/config/services/idpay/payments'] = array(
    'title' => 'IDPay payments',
    'page callback' => 'idpay_payments_page',
    'access arguments' => array('access administration pages'),
    'file path' => drupal_get_path('module', 'idpay'),
    'file' => 'idpay.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function idpay_permission() {
  return array(
    'access idpay payment' => array(
      'title' => t('Access idpay payment'),
      'description' => t('Use idpay gateway for new payment.')
    )
  );
}

/**
 * Implements hook_init().
 *
 * Nags the user about the missing library on IDPay admin pages.
 */
function idpay_init() {
  $item = menu_get_item();
  if ($item['access'] && strpos($item['path'], 'admin/config/services/idpay') === 0) {
    $path = idpay_get_library_path();

    // Check for the existence of one file from the library.
    if (!$path || !file_exists($path . '/src/Idpay.php')) {
      $message = t('The IDPay API library is required for the IDPay module to function.
        Download the library from <a href="https://github.com/vispamir/idpay" target="_blank">GitHub</a> and place it in <em>!path</em>.', array('!path' => $path));
      drupal_set_message($message, 'error');
    }
    
    if (!file_exists($path . '/vendor/autoload.php')) {
      $message = t('Not found IDPay dependencies as vendor directory.
        Please go to ./sites/all/libraries/idpay and run `composer install`.');
      drupal_set_message($message, 'error');
    }
  }
}

/**
 * Implements hook_libraries_info().
 */
function idpay_libraries_info() {
  $libraries['idpay'] = array(
    'name' => 'IDPay API',
    'vendor url' => 'https://github.com/vispamir/idpay',
    'xautoload' => function($api) {
      $api->namespaceRoot('Idpay', 'src');
    },
  );

  return $libraries;
}

/**
 * Implements hook_xautoload().
 *
 * Register idpay library classes if libraries module is not
 * enabled.
 */
function idpay_xautoload($api) {
  if (module_exists('libraries')) {
    return;
  }
  $api->setExtensionDir(idpay_get_library_path());
  $api->namespaceRoot('Idpay', 'src');
}

/**
 * Returns the filesystem path to the IDPay API library.
 */
function idpay_get_library_path() {
  $path = 'sites/all/libraries/idpay';
  // If installed, use the Libraries API to locate the library.
  if (module_exists('libraries')) {
    module_load_include('module', 'libraries');
    $path = libraries_get_path('idpay');
  }

  return $path;
}